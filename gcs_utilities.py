from google.cloud import storage
import os
import logging

logger = logging.getLogger(__name__)


def get_gcs_path_components(file_name):
    if not file_name.startswith('gs://'):
        return None, None
    parts = file_name.replace("gs://", "").split("/")
    if len(parts) < 2:
        raise RuntimeError(f'GCS path needs bucket and path to folder: {file_name}')

    bucket = parts.pop(0)
    path = "/".join(parts)
    return bucket, path


def check_if_file_exists(gcs_path):
    bucket_name, path = get_gcs_path_components(gcs_path)
    try:
        client = storage.Client()
        bucket = client.get_bucket(bucket_name)
        return storage.Blob(bucket=bucket, name=path).exists(client)
    except Exception as e:
        logger.error(f'Failed to locate GCP bucket: {e}')
        raise RuntimeError(f'Failed to locate GCP bucket: {e}')


def download_gcs_file(from_gcs_path, to_local_path):
    bucket_name, path = get_gcs_path_components(from_gcs_path)
    try:
        client = storage.Client()
        bucket = client.get_bucket(bucket_name)
    except Exception as e:
        logger.error(f'Failed to locate GCP bucket: {e}')
        raise RuntimeError(f'Failed to locate GCP bucket: {e}')

    try:
        blob = bucket.get_blob(path)
        blob.download_to_filename(to_local_path)
    except Exception as e:
        logger.error(f'Failed to download {from_gcs_path}: {e}')
        raise RuntimeError(f'Failed to download {from_gcs_path}: {e}')


def upload_gcs_file(gcs_path, file):
    bucket_name, path = get_gcs_path_components(gcs_path)
    try:
        client = storage.Client()
        bucket = client.get_bucket(bucket_name)
    except Exception as e:
        logger.error(f'Failed to locate GCP bucket: {e}')
        raise RuntimeError(f'Failed to locate GCP bucket: {e}')

    blob = bucket.blob(path)
    blob.upload_from_filename(file, timeout=300)


def list_blobs(bucket, prefix=None):
    return storage.Client().list_blobs(bucket, prefix=prefix)


def move_folder_to_gcs(folder, bucket):
    files = os.listdir(folder)
    for file in files:
        upload_gcs_file(f'{bucket}/{file}', f'{folder}/{file}')


def get_bucket_blob_names(bucket):
    input_bucket_name, input_prefix = get_gcs_path_components(bucket)
    blobs = list_blobs(input_bucket_name, input_prefix)

    blob_names = [blob.name.replace(input_prefix + '/', '') for blob in blobs]
    return [name for name in blob_names if name]
