ssn_zoro_number_by_catalog = [
    {'index': 0, 'key': 'supplierStockNumber', 'p360Name': 'Article.SupplierAID'},
    {'index': 1, 'key': 'zoroNumber', 'p360Name': 'ZORO_HeaderData_07.ZoroItemNo'},
]





# This is an old format fields dict that might be useful in the future. Keeping it here for now
# fields = {
#     "general": {
#         'endpoint': 'list/Article/bySearch',
#         'fields': [
#             {'index': 0, 'display': 'Supplier Stock Number',
#              'name': 'ArticleSupplierRelation.SupplierArticleAid("<Main Supplier>")'},
#             {'index': 1, 'display': 'Main Supplier', 'name': 'Article.MainSupplier', 'responseObjectKey': 'label'},
#             {'index': 2, 'display': 'Grainger Supplier Name', 'name': 'ZORO_HeaderData_02.GraingerSupplierName'},
#             {'index': 3, 'display': 'Grainger Supplier Number', 'name': 'ZORO_HeaderData_02.GraingerSupplierNo'},
#             {'index': 4, 'display': 'Zoro Number', 'name': 'Article.SupplierAID'},
#             {'index': 5, 'display': 'Current Status', 'name': 'Article.CurrentStatus'},
#             {'index': 6, 'display': 'Brand Name', 'name': 'ZORO_Article.BrandName'},
#             {'index': 7, 'display': 'Manufacturer Model Number', 'name': 'Article.SupplierAltAID'},
#             {'index': 8, 'display': 'Is Discontinued', 'name': 'ZORO_HeaderData_03.IsDiscontinued'},
#             {'index': 9, 'display': 'Is Public', 'name': 'ZORO_HeaderData_01.IsPublic'},
#             {'index': 10, 'display': 'Is Restricted', 'name': 'ZORO_HeaderData_01.IsRestricted'},
#             {'index': 11, 'display': 'RMC', 'name': 'ZORO_HeaderData_04.RMC'},
#             {'index': 12, 'display': 'Sales Status', 'name': 'ZORO_HeaderData_01.SalesStatus'},
#             {'index': 13, 'display': 'Status', 'name': 'Article.CurrentStatus'},
#             {'index': 14, 'display': 'Brand', 'name': 'ZORO_Article.BrandName'},
#         ]
#     }
# }
