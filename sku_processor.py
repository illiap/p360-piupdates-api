import re
import os
import openpyxl
import datetime
import uuid

from itertools import groupby
from flask import Flask, request
from requests import get, post
from http import HTTPStatus

import p360_api_config


class SkuProcessor:
    def __init__(self, token, api_url):
        self.errors = []
        self.status_code = 200
        self.api_url = api_url
        self.token = token

    def interrupt_with_errors(self, message, status_code):
        self.errors.append(message)
        self.status_code = status_code
        raise SkuProcessorInterruptException()

    def validate_token(self):
        if not self.token:
            self.interrupt_with_errors("Token is invalid. Please use the correct credentials", HTTPStatus.UNAUTHORIZED)

    def build_sku_data_from_spreadsheet(self, sheet):
        sku_data = {}

        for row in sheet.iter_rows(min_row=2, max_row=sheet.max_row):
            ssn = str(row[1].value).strip() if row[1].value else None
            zoro_number = str(row[2].value).strip() if row[2].value else None
            catalog = str(row[0].value).strip() if row[0].value else None
            provided_identifier = ssn if ssn else zoro_number

            if not ssn and not zoro_number:
                self.interrupt_with_errors(
                    f"Item No. or Zoro No. is required on row {str(row[0].row)}.",
                    HTTPStatus.BAD_REQUEST
                )

            if not catalog:
                self.interrupt_with_errors(f"Catalog is required on row {str(row[0].row)}.", HTTPStatus.BAD_REQUEST)

            item = {
                "catalog": catalog,
                "supplierStockNumber": ssn,
                "zoroNumber": zoro_number,
                "isDiscontinued": row[3].value,
                "isPublic": row[6].value,
                "comment": row[10].value,
                "zoroCost": row[22].value,
                "zoroCostValidFrom": row[23].value,
                "zoroCostValidTo": row[24].value,
            }

            if catalog not in sku_data:
                sku_data[catalog] = {}

            if provided_identifier in sku_data[catalog]:
                self.interrupt_with_errors(f"Found a SKU duplicate on row {str(row[0].row)}.", HTTPStatus.BAD_REQUEST)

            sku_data[catalog][provided_identifier] = item

        if not sku_data:
            self.interrupt_with_errors(
                "Could not find any SKU data in the provided Excel Spreadsheet.",
                HTTPStatus.BAD_REQUEST
            )

        return sku_data

    @staticmethod
    def search_sku_data(sku_data, catalog, ssn, zoro_number):
        by_ssn = sku_data.get((catalog, ssn), None)
        by_zoro_number = sku_data.get((catalog, zoro_number), None)
        return (by_ssn, (catalog, ssn)) if by_ssn else (by_zoro_number, (catalog, zoro_number))

    def complete_sku_data(self, sku_data, skus_found):
        for catalog in skus_found:
            if catalog not in sku_data:
                self.interrupt_with_errors("P360 API returned an unexpected catalog.",
                                           HTTPStatus.INTERNAL_SERVER_ERROR)

            for sku in skus_found[catalog]:
                if sku['supplierStockNumber'] in sku_data[catalog]:
                    sku_data[catalog][sku['supplierStockNumber']]['found'] = True
                    sku_data[catalog][sku['supplierStockNumber']]['zoroNumber'] = sku['zoroNumber']
                if sku['zoroNumber'] in sku_data[catalog]:
                    sku_data[catalog][sku['zoroNumber']]['found'] = True
                    sku_data[catalog][sku['zoroNumber']]['supplierStockNumber'] = sku['supplierStockNumber']

        return sku_data

    def validate_sku_data(self, sku_data):
        skus_found, status_code = self.find_skus(sku_data, p360_api_config.ssn_zoro_number_by_catalog)
        invalid_skus = []
        valid_skus = {}

        if status_code == HTTPStatus.OK:
            sku_data = self.complete_sku_data(sku_data, skus_found)
            for catalog in sku_data:
                for sku in sku_data[catalog].values():
                    if sku.get('found', False):
                        invalid_reasons = self.validate_sku_field_values(sku)
                        if invalid_reasons:
                            invalid_skus += invalid_reasons
                        else:
                            if catalog not in valid_skus:
                                valid_skus[catalog] = []
                            valid_skus[catalog].append(sku)
                    else:
                        reason_string = f'{"SSN" if sku["supplierStockNumber"] else "Zoro Number"} ' \
                                        f'{sku["supplierStockNumber"] if sku["supplierStockNumber"] else sku["zoroNumber"]}'
                        invalid_skus.append({'sku': sku, 'reason': f"Could not find sku {reason_string}"
                                                                   f" in catalog {sku['catalog']}"})
        else:
            self.interrupt_with_errors(
                "API call to P360 failed when trying to validate your SKUs. Please contact your administrator",
                HTTPStatus.INTERNAL_SERVER_ERROR
            )

        return valid_skus, invalid_skus

    def find_skus(self, input_sku_data, fields):
        sku_data = {}
        status_code = HTTPStatus.OK
        for catalog_name in input_sku_data:
            for chunk in chunker(list(input_sku_data[catalog_name].values()), 100):
                query = []
                for item in chunk:
                    if item['supplierStockNumber']:
                        query.append(f'Article.SupplierAID = "{item["supplierStockNumber"]}"')
                    elif item['zoroNumber']:
                        query.append(f'ZORO_HeaderData_07.ZoroItemNo = "{item["zoroNumber"]}"')

                data, p360_status_code = self.call_p360('list/Article/bySearch', " OR ".join(query), catalog_name, fields)
                if p360_status_code != HTTPStatus.OK:
                    status_code = p360_status_code
                    break
                sku_data[catalog_name] = data

        return sku_data, status_code

    def call_p360(self, endpoint, query, catalog, fields):
        sku_data = []
        try:
            response = get(
                f"{self.api_url}/{endpoint}",
                params={
                    "cacheId": "no-cache",
                    "catalog": catalog,
                    "query": query,
                    "fields": ",".join([field['p360Name'] for field in fields]),
                },
                headers={"Authorization": f"Basic {request.args.get('token')}"}
            )

            if "rows" not in response.json():
                return {}, response.status_code
        except Exception:
            self.interrupt_with_errors(
                "API call to P360 failed. Please contact your administrator",
                HTTPStatus.INTERNAL_SERVER_ERROR
            )

        for row in response.json()['rows']:
            item = {}
            for field in fields:
                item[field['key']] = row['values'][field['index']]
            sku_data.append(item)

        return sku_data, response.status_code

    @staticmethod
    def build_output_spreadsheets(sku_data):
        app_root = os.path.dirname(os.path.abspath(__file__))
        tmp_path = f'{app_root}/tmp_{uuid.uuid4().hex[:6]}'
        folder_path = f'{tmp_path}/output'
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)

        for catalog_key, skus_in_catalog in sku_data.items():
            current_template_row = 2
            template_wb = openpyxl.load_workbook(f'{app_root}/template.xlsx')
            template_sheet = template_wb['Supplier Template']

            for sku in skus_in_catalog:
                template_sheet[f'A{current_template_row}'] = sku['supplierStockNumber']
                template_sheet[f'B{current_template_row}'] = sku['isDiscontinued']
                template_sheet[f'E{current_template_row}'] = sku['isPublic']
                template_sheet[f'I{current_template_row}'] = sku['comment']
                template_sheet[f'U{current_template_row}'] = sku['zoroCost']
                template_sheet[f'V{current_template_row}'] = sku['zoroCostValidFrom'].strftime('%m/%d/%y') if sku[
                    'zoroCostValidFrom'] else sku['zoroCostValidFrom']
                template_sheet[f'W{current_template_row}'] = sku['zoroCostValidTo'].strftime('%m/%d/%y') if sku[
                    'zoroCostValidTo'] else sku['zoroCostValidTo']

                current_template_row += 1

            template_wb.save(f'{folder_path}/{catalog_key}.xlsx')

        return tmp_path

    @staticmethod
    def find_value_in_sku_fields(sku_fields, value):
        for field in sku_fields:
            if field['display'] == value:
                return field['value']

        return None

    @staticmethod
    def validate_sku_field_values(sku):
        reasons = []

        if sku['isDiscontinued'] and str(sku['isDiscontinued']).lower() not in ['yes', 'no', 'y', 'n']:
            reasons.append({
                'sku': sku,
                'reason': f"Value {sku['isDiscontinued']} is not valid for Is Discontinued field"
            })

        if sku['isPublic'] and str(sku['isPublic']).lower() not in ['yes', 'no', 'y', 'n']:
            reasons.append({
                'sku': sku,
                'reason': f"Value {sku['isPublic']} is not valid for Is Public field"
            })

        if sku['zoroCost']:
            try:
                float(sku['zoroCost'])
            except Exception:
                reasons.append({
                    'sku': sku,
                    'reason': f"Value {sku['zoroCost']} is not valid for Product Cost"
                })

        if sku['zoroCostValidFrom'] and not isinstance(sku['zoroCostValidFrom'], datetime.date):
            try:
                datetime.datetime.strptime(str(sku['zoroCostValidFrom']), "%m/%d/%Y")
            except Exception:
                reasons.append({
                    'sku': sku,
                    'reason': f"Value {sku['zoroCostValidFrom']} is not valid for Zoro Cost: Valid From."
                              f" The correct format is mm/dd/yyyy"
                })

        if sku['zoroCostValidTo'] and not isinstance(sku['zoroCostValidTo'], datetime.date):
            try:
                datetime.datetime.strptime(str(sku['zoroCostValidTo']), "%m/%d/%Y")
            except Exception:
                reasons.append({
                    'sku': sku,
                    'reason': f"Value {sku['zoroCostValidTo']} is not valid for Zoro Cost: Valid To."
                              f" The correct format is mm/dd/yyyy"
                })

        return reasons


class SkuProcessorInterruptException(Exception):
    pass


def merge_sku_dicts(a, b):
    if not a:
        return b

    for key in a:
        a[key] += b[key]

    return a


def chunker(seq, size):
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))
