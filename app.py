import openpyxl
import os
import gcs_utilities
import shutil
import uuid

from flask import Flask, request
from requests import get, post
from http import HTTPStatus
from sku_processor import SkuProcessor, SkuProcessorInterruptException
from flask import send_file

app = Flask(__name__)
api_url = "https://zoro-qa.p360.informaticahosted.com/rest/V1.0"


@app.route("/checkToken")
def check_token():
    response = get(
        f"{api_url}/list/Article/byCatalog",
        params={"cacheId": "no-cache", "catalog": "Grainger"},
        headers={"Authorization": f"Basic {request.args.get('token')}"}
    )

    return {"success": True if response.status_code == HTTPStatus.OK else False}, response.status_code


@app.route("/processSpreadsheet", methods=['POST'])
def process_spreadsheet():
    # skip_validation = request.args.get('skipValidation')
    if 'file' in request.files:
        try:
            file = request.files['file']
            wb = openpyxl.load_workbook(file)
            if 'Supplier Template' in wb.sheetnames:
                sheet = wb['Supplier Template']
            else:
                return {
                   "errors": ["Could not find 'Supplier Template' sheet in your Excel file"]
                }, HTTPStatus.BAD_REQUEST
        except Exception:
            return {
                "errors": ["Cannot open your excel file, please verify that you provided the correct file"]
            }, HTTPStatus.BAD_REQUEST

        sku_processor = SkuProcessor(request.args.get('token'), api_url)
        try:
            sku_processor.validate_token()
            sku_data = sku_processor.build_sku_data_from_spreadsheet(sheet)
            valid_skus, invalid_skus = sku_processor.validate_sku_data(sku_data)
            folder_path = sku_processor.build_output_spreadsheets(valid_skus)
            zip_name = upload_spreadsheets_to_hotfolder(folder_path)
        except SkuProcessorInterruptException:
            return {"errors": sku_processor.errors}, sku_processor.status_code
    else:
        return {"errors": ["Please upload a file"]}, HTTPStatus.BAD_REQUEST

    return {"successful": valid_skus, "failed": invalid_skus, "zipName": zip_name}, HTTPStatus.OK


@app.route("/downloadZip", methods=['GET'])
def download_zip():
    # os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/Users/xixp022/.gcp/zoro-platform-sb-60862dfe80d5.json"
    app_root = os.path.dirname(os.path.abspath(__file__))
    local_path = f'{app_root}/downloads'

    if os.path.exists(local_path):
        shutil.rmtree(local_path)

    os.makedirs(local_path)

    zip_name = request.args.get('zipName')
    bucket_path = f'gs://illia-test-bucket/piupdate/{zip_name}.zip'
    local_file_path = f'{local_path}/{zip_name}.zip'
    gcs_utilities.download_gcs_file(bucket_path, local_file_path)

    return send_file(local_file_path, as_attachment=True)


def upload_spreadsheets_to_hotfolder(folder_path):
    # os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/Users/xixp022/.gcp/zoro-platform-sb-60862dfe80d5.json"
    zip_name = f'{uuid.uuid4().hex}'
    local_zip_path = f'{folder_path}/{zip_name}'
    bucket_path = f'gs://illia-test-bucket/piupdate/{zip_name}.zip'
    shutil.make_archive(f'{local_zip_path}', 'zip', f'{folder_path}/output')

    gcs_utilities.upload_gcs_file(bucket_path, f'{local_zip_path}.zip')
    shutil.rmtree(folder_path)

    return zip_name


def unpack_sku_data(data, search_by):
    seen = {}
    need_review = {}
    ready_to_process = {}

    for sku_key in data:
        for field in data[sku_key]:
            if field['display'] == search_by:
                if field['value'] in seen:
                    need_review[sku_key] = data[sku_key]
                    # the first element found will be in the `seen` dict
                    first_found = seen[field['value']]
                    # we only need to add the first found to `need_review` once
                    # in case there are more than two duplicates
                    if first_found['sku_key'] not in need_review:
                        need_review[first_found['sku_key']] = first_found['field']
                else:
                    seen[field['value']] = {'sku_key': sku_key, 'field': data[sku_key]}
                    ready_to_process[sku_key] = data[sku_key]
                break

    return ready_to_process, need_review


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


@app.route("/")
def hello_world():
    data = {"name": "Hello world!"}
    return data, HTTPStatus.OK
